package DAO;

import Rout.Rout;

import java.util.ArrayList;
import java.util.List;

public class CollectionRoutDao {
    private List<Rout> routs = new ArrayList<>();

    public void createRoutList(int size){
        for (int i = 0; i < size; i++) {
            routs.add(new Rout().createRout());
        }

    }

    @Override
    public String toString() {
        return "********** LIST OF ROUTS **********" +
                routs +
                "\n********** END LIST OF ROUTS **********";
    }

    public List<Rout> getRouts() {
        return routs;
    }
}
