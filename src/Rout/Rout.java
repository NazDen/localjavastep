package Rout;

import Enum.City;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Rout implements RoutGenerator{
    private City departure = City.KIEV;
    private City arrival = City.UNKNOWN;
    private String id;
    private Date invisibleDate;
    private Date time;
    private Date date;
    private static int countId = 100;
    private int freeSeats;
    private SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
    private SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

    public Rout(){
    }

    public Rout(City departure, City arrival, Date invisibleDate) {
        this.departure = departure;
        this.arrival = arrival;
        this.id = this.createId(this.departure, this.arrival);
        this.invisibleDate = invisibleDate;
        this.time = invisibleDate;
        timeFormat.format(invisibleDate);
        this.date = invisibleDate;
        dateFormat.format(invisibleDate);
        this.freeSeats = this.actualCapacity();
    }

    @Override
    public Rout createRout() {
        this.freeSeats = this.actualCapacity();
        invisibleDate= createRandomDate();
        this.time = invisibleDate;
        timeFormat.format(invisibleDate);
        this.date = invisibleDate;
        dateFormat.format(invisibleDate);
        this.departure = departure;
        this.arrival = createArivalCity();
        this.id = this.createId(this.departure, this.arrival);
        return this;
    }

    public City getDeparture() {
        return departure;
    }

    public void setDeparture(City departure) {
        this.departure = departure;
    }

    public City getArrival() {
        return arrival;
    }

    public void setArrival(City arrival) {
        this.arrival = arrival;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getInvisibleDate() {
        return invisibleDate;
    }

    public void setInvisibleDate(Date invisibleDate) {
        this.invisibleDate = invisibleDate;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public static int getCountId() {
        return countId;
    }

    public int getFreeSeats() {
        return freeSeats;
    }

    public void setFreeSeats(int freeSeats) {
        this.freeSeats = freeSeats;
    }

    public SimpleDateFormat getTimeFormat() {
        return timeFormat;
    }

    public void setTimeFormat(SimpleDateFormat timeFormat) {
        this.timeFormat = timeFormat;
    }

    public SimpleDateFormat getDateFormat() {
        return dateFormat;
    }

    public void setDateFormat(SimpleDateFormat dateFormat) {
        this.dateFormat = dateFormat;
    }

    public static Date createRandomDate(){
        long currentTimeMill = new Date().getTime();
        long result = currentTimeMill + (long)(Math.random()*604800000);
        Date date = new Date(result);
        return date;
    }


    public String createId(City departure, City arrival) {
        char firstLetter = departure.toString().charAt(0);
        char secondLetter = arrival.toString().charAt(0);
        countId++;
        String resultId = String.valueOf(firstLetter) +  String.valueOf(secondLetter) + "-" + countId;
        return resultId;
    }

    public int actualCapacity(){
        int random = (int) (Math.random()*150);
        int result = 150 - random;
        return result;
    }


    public City createArivalCity (){
        Calendar calendar= Calendar.getInstance();
        calendar.setTime(this.getDate());
        int dayOfWeek= calendar.get(Calendar.DAY_OF_WEEK);
        City result= null;
        switch (dayOfWeek){
            case 1:
                result= City.LONDON ;
                break;
            case 2:
                result= City.MADRID;
                break;
            case 3:
                result= City.MOSCOW;
                break;
            case 4:
                result= City.ROME;
                break;
            case 5:
                result= City.BERLIN;
                break;
            case 6:
                result= City.BARCELONA;
                break;
            case 7:
                result= City.PARIS;
                break;
            default:
                result= City.UNKNOWN;

        }
        return  result;
    }



    @Override
    public String toString() {
        return "\n\t========== ROUT ==========\n" +
                "ID:\t  "  + id + "\t\t Free seats: " + actualCapacity() +
                "\nDate: " + dateFormat.format(date) + "\t Departure:  " + departure +
                "\nTime: " + timeFormat.format(time) + "\t\t\t Arrival:    " + arrival +
                "\n\t========== END ==========";
    }


}
